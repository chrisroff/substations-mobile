import React from 'react';
import OAuth from './components/oauth';
import Substations from './components/substations';
import Substation from './components/substation';
import AddSubstation from './components/addSubstation';
import { StackNavigator,addNavigationHelpers } from "react-navigation";
import { connect } from 'react-redux';
import {deleteSubstation, fetchSubstationsFromAPI} from './actions';
    
export const AppNavigator = StackNavigator({
    Home: { screen: OAuth },
    Show:{screen: Substation},
    Add: { screen: AddSubstation }
});

const AppWithNavigationState = ({ dispatch, nav }) =>{ 
    const navState= addNavigationHelpers({ dispatch, state: nav });
    navState.substation = nav.substation;
    
   return (
    <AppNavigator navigation={navState} />
)};

const mapStateToProps = state => ({
    nav: state.navigation,
});



export default connect(mapStateToProps)(AppWithNavigationState);