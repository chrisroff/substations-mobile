import {SHOW_SUBSTATION} from '../constants';

const initialState ={
    show_substation:{}
}

export default function substationReducer(state = initialState,action){
    switch(action.type){
        case SHOW_SUBSTATION :
        return {
            ...state,
            show_substation:action.data
        }
        default :
        return state;
    }
}