import {AUTHENTICATED, ADD_AUTH_CODE} from '../constants';

const initialState ={
    authenticated:true,
    auth_code:'ya29.GlvMBAZe54JBEPBj3yKtBC3wBP0zllCSfCfcAHkWwy2S2K4PHfLexBZVl8Mwl1X1Oc_ixcNDzg46avtIb1Hgd14U0Ng6vRbqM2reG0N0TPsnmrYMdjJQBighgjZK'
}

export default function oauthReducer(state = initialState, action){
    switch(action.type){
        case AUTHENTICATED :
        return {
            ...state,
            authenticated: action.data ? true : false,
            access_token: action.data
        }
        case ADD_AUTH_CODE :
        return {
            ...state,
            auth_code:action.data
        }
        default :
        return state
    }
}