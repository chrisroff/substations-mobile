import {AppNavigator} from '../navigation';
import {SHOW_SUBSTATION} from '../constants';

const initialNavState = AppNavigator.router.getStateForAction(AppNavigator.router.getActionForPathAndParams('Home'));

export default function navReducer(state = initialNavState, action){
    let newState = AppNavigator.router.getStateForAction(action, state);
    
    if(action.type === SHOW_SUBSTATION ){
        newState.substation = action.data;        
    }

    return newState || state;
}
