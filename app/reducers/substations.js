import {FETCHING_SUBSTATIONS, FETCHING_SUBSTATIONS_SUCCESS, FETCHING_SUBSTATIONS_FAILURE, DELETE_SUBSTATION} from '../constants';

const initialState ={
    substations:[],
    isFetching: false,
    error: false
}

export default function substationReducer(state = initialState, action){
    switch(action.type){
        case FETCHING_SUBSTATIONS :
        return {
            ...state,
            isFetching:true,
            error:false,
        }
        case FETCHING_SUBSTATIONS_SUCCESS : 
        return {
            ...state,            
            isFetching:false,
            error: false,
            substations: action.data
        }
        case FETCHING_SUBSTATIONS_FAILURE :
        return {
            ...state,
            isFetching: false,
            substations: [],
            error:true,
            errorData: action.data
        }
        default :
        return state;
    }
}