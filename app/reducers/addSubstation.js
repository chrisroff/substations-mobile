import {ADD_SUBSTATION_FIELD, ADD_NEW_SUBSTATION} from '../constants';

const initialState ={
    field_data:{
        PrimarySubstationName:"Forbury",
        Longitude:"-0.965866",
        Latitude:"51.457751",
        CorrespondingBSP:"Caversham",
        ContactTitle:"Commercial Contract Manager",
        ContactName:"Steve Johnson",
        ContactEmail:"Steve.Johnson@sse.com",
        ContactTel:"02392 624205",
        VoltagekV:"22/11",
        TransformerNameplateRatingMVA:"6.00",
        FaultlevelkA:"13.10",
        MinimumLoadMW:"0.80",
        MaximumLoadMW:"5.30",
        AvailableHeadroomCapacityMVA:"6.80",
        ReversePowerflowCapability:"50",
        CorrespondingGSP : "",
        ContractedGenerationMVA : "",
        TransmissionStatus:"Constrained",
        TransmissionWorks:"",
        TransmissionReinforcementCompletionDate:"",
        DistributionWorks:"",
        DistributionReinforcementCompletionDate:""
    }
}

export default function addSubstationReducer(state = initialState, action){
    switch(action.type){
        case ADD_SUBSTATION_FIELD :
        
        let newFieldData ={ 
            ...state.field_data
        };

        newFieldData[action.data.fieldName] = action.data.value;
        const completeFieldData = addAdditionalFields(newFieldData);
        console.log("new substation " + JSON.stringify(completeFieldData));
        return {
            ...state,
            field_data: completeFieldData
        }

       /* case ADD_NEW_SUBSTATION :
        return{
            ...state,
            field_data: {}
        } */
        default :
        return state
    }
}

function addAdditionalFields(fieldData){
    const additionalFields = [        
        "CorrespondingGSP",
        "ContractedGenerationMVA",
        "TransmissionStatus",
        "TransmissionWorks",
        "TransmissionReinforcementCompletionDate",
        "DistributionWorks",
        "DistributionReinforcementCompletionDate",
        ];

        additionalFields.forEach((field) =>{
            fieldData[field] = "";
        });

        return fieldData;

    
}