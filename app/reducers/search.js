import {SEARCH, DELETE_SUBSTATION} from '../constants';

const initialState ={
    search_text:""
}

export default function searchReducer(state = initialState, action){
    switch(action.type){
        case SEARCH :
        return {
            ...state,
            search_text: action.data
        }
        default :
        return state
    }
}