import {combineReducers} from 'redux';
import substations from './substations';
import substation from './substation';
import oauth from './oath';
import search from './search';
import navigation from './nav';
import add_substation from './addSubstation';



const rootReducer = combineReducers({
    substations,
    substation,
    oauth,
    search,
    add_substation,
    navigation
});

export default rootReducer;

