import { Linking } from 'react-native';
import {ThunkAction} from 'redux-thunk';
import { NavigationActions } from 'react-navigation';
import {
    gettingSubstations,
    gotSubstations,
    getSubstationsFailure,
    onAuthenticated,
    onSearch,
    onAddNew,
    onAddSubstationField,
    onShowSubstation,
    onAddAuthCode
} from './events';

//const apiAddress = 'http:/169.254.80.80:3001/api/substation';
const apiAddress = 'http://apitest01.northeurope.cloudapp.azure.com:3003/api/substation';


export function fetchSubstationsFromAPI(){
    return (dispatch, getState) =>{

        const rootState = getState();

        const options = {
            headers : {
                'Content-Type' : 'application/json',
                'Accept' : 'application/json',
                'Authorization' : `Bearer ${rootState.oauth.access_token}`
            }
        }
        dispatch(gettingSubstations());
        fetch(`${apiAddress}?name=${rootState.search.search_text}`,options)
        .then(res => {
            return res.json();
        })
        .then(json =>{ 
            dispatch(gotSubstations(json));
         })
        .catch((err) =>{
            console.log("got error " + err);
            dispatch(getSubstationsFailure(err));});
    }
}

export function addNewSubstation(){
    return (dispatch,getState) => {
        const rootState = getState();
        const bodyJson = JSON.stringify(rootState.add_substation.field_data);

        fetch(apiAddress, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization' : `Bearer ${rootState.oauth.access_token}`
            },
            body: bodyJson})
          .then(result => {
              if(result.status === 200){
                dispatch(onAddNew());
                dispatch(NavigationActions.back());
              }
              else{
                  console.log("got error " + JSON.stringify(result));
              }
            })
          .catch(err => console.log("got error " +  err));

    }
}

export function setSearch(search){
    return (dispatch) =>{
        dispatch(onSearch(search));
    }
}

export function addSubstationField(fieldName,value){
    return (dispatch) => {
        dispatch(onAddSubstationField({
            fieldName,
            value
        }));
    }
}

export function showSubstation(substation){
    return (dispatch) =>{
        dispatch(onShowSubstation(substation));
    }
}

export function deleteSubstation(substationName){
    return (dispatch,getState) =>{
        const rootState = getState();
        fetch(`${apiAddress}/${substationName}`, {
            method: 'DELETE',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization' : `Bearer ${rootState.oauth.access_token}`
            }})
          .then(result => {
              if(result.status !== 200){
                console.log("got error deleting substation" + JSON.stringify(result));
              }
            })
          .catch(err =>console.log("got error deleting substation" + err));
    }
}

export function addAuthCode(authCode){
    return (dispatch)=>{
        dispatch(onAddAuthCode(authCode));
    }
}

export function getAccessToken(){
    return (dispatch, getState) =>{

        const rootState = getState();


        const client_id = "978921633675-lsfa11bovl5nhaadrs9j12q03h64rm81.apps.googleusercontent.com";
        let auth_url = "https://accounts.google.com/o/oauth2/token";

        
        const options = {
        method: 'POST',
        body: `grant_type=authorization_code&code=${rootState.oauth.auth_code}&client_id=${client_id}&redirect_uri=urn:ietf:wg:oauth:2.0:oob`,
        headers:{
            'Content-Type': 'application/x-www-form-urlencoded'
        }};
        
        fetch(auth_url,options)
        .then(result =>{
            return result.json();
        })
        .then(response =>{
            if(response.access_token){
                dispatch(onAuthenticated(response.access_token));
            }
            else{
                //dispatch(onAuthenticationFailure());
                console.log("Error getting access token " + response.error_description);
            }
        })
        .catch(err => console.error(err));
    }
}