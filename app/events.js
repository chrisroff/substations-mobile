import {
    FETCHING_SUBSTATIONS, 
    FETCHING_SUBSTATIONS_SUCCESS, 
    FETCHING_SUBSTATIONS_FAILURE, 
    AUTHENTICATED, SEARCH, 
    ADD_NEW_SUBSTATION, 
    ADD_SUBSTATION_FIELD,
    SHOW_SUBSTATION,
    ADD_AUTH_CODE
} from './constants';

export function gettingSubstations(){
    return{
        type:FETCHING_SUBSTATIONS
    }
};

export function gotSubstations(data){
    return {
        type:FETCHING_SUBSTATIONS_SUCCESS,
        data
    }
};

export function getSubstationsFailure(data){
    return {
        type:FETCHING_SUBSTATIONS_FAILURE,
        data
    }
};

export function onAuthenticated(data){
    return {
        type:AUTHENTICATED,
        data
    }
}

export function onSearch(data){
    return{
        type:SEARCH,
        data
    }
}

export function onAddNew(){
    return{
        type:ADD_NEW_SUBSTATION
    }
}

export function onAddSubstationField(data){
    return {
        type:ADD_SUBSTATION_FIELD,
        data
    }
}

export function onShowSubstation(data){
    return{
        type:SHOW_SUBSTATION,
        data
    }
}

export function onAddAuthCode(data){
    return{
        type:ADD_AUTH_CODE,
        data
    }
}

