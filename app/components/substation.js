import React from 'react';
import { StyleSheet, View} from 'react-native';
import { Container, Header, Content, List, ListItem, Text, Icon, Left,Right, Body, Button,Card,CardItem, Fab } from 'native-base'; 
import {connect} from 'react-redux';
import MapView from 'react-native-maps';
import {deleteSubstation, fetchSubstationsFromAPI} from '../actions';
import { NavigationActions } from "react-navigation";

const styles = StyleSheet.create({
    name:{
        fontWeight:'bold'
    },
    label:{
        color:'gray'
    },
    text:{
        height:10
    },
    map: {
        ...StyleSheet.absoluteFillObject
      }
});

const {
    label,
    name,
    text,
    map
} = styles;

class Substation extends React.Component {
    constructor(props){
        super(props);
    }

    render() {

        return (
            <Container>
                <Card>
                <CardItem style={{height:180}}>
                <MapView
                    style ={map}
                    region={{
                    latitude: parseFloat(this.props.substation.Latitude),
                    longitude: parseFloat(this.props.substation.Longitude),
                    latitudeDelta: 0.004,
                    longitudeDelta: 0.004,
                        }}
                    >
                    <MapView.Marker
                        title ={this.props.substation.PrimarySubstationName}
                        coordinate={{latitude: parseFloat(this.props.substation.Latitude),longitude:parseFloat(this.props.substation.Longitude)}}
                        centerOffset={{ x: -18, y: -60 }}
                        anchor={{ x: 0.69, y: 1 }}
                     />
                     </MapView>
                </CardItem>
                <CardItem><Left><Text style={label}>Contact:</Text></Left><Body><Text>{this.props.substation.ContactName}</Text></Body></CardItem>
                <CardItem><Left><Text style={label}>Telephone:</Text></Left><Body><Text>{this.props.substation.ContactTel}</Text></Body></CardItem>
                <CardItem><Left><Text style={label}>Status:</Text></Left><Body><Text>{this.props.substation.TransmissionStatus}</Text></Body></CardItem>
                <CardItem><Left><Text style={label}>Voltage KV:</Text></Left><Body><Text>{this.props.substation.VoltagekV}</Text></Body></CardItem>
                <CardItem><Left><Text style={label}>Rating:</Text></Left><Body><Text>{this.props.substation.TransformerNameplateRatingMVA}</Text></Body></CardItem>
                <CardItem><Left><Text style={label}>Fault Level KA:</Text></Left><Body><Text>{this.props.substation.FaultlevelkA}</Text></Body></CardItem>
            </Card>
            <Fab
                    position="bottomRight"
                    onPress={() =>this.props.delete_substation(this.props.substation.PrimarySubstationName)}>
                    <Icon name="trash" />
                </Fab>
          </Container>
        )
    }
}

Substation.navigationOptions = props => {
    const { navigation } = props;
    return{
        headerTitle:navigation.substation.PrimarySubstationName
    }
}

function mapStateToProps(state){
    return{
        substation : state.substation.show_substation
    }
}

function mapDispatchToProps(dispatch){
    return{
    delete_substation : (name) => {
        dispatch(deleteSubstation(name));
        dispatch(fetchSubstationsFromAPI());
        dispatch(NavigationActions.back());
    }
}
}


export default connect(mapStateToProps,mapDispatchToProps)(Substation);



