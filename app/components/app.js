import React from 'react';
import {
    View
} from 'react-native';
import OAuth from './oauth';
import Substations from './substations';

class App extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return(
        <View>
            <Substations/>
        </View>
        );
    }
}

export default App;