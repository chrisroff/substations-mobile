import React from 'react';
import {
    Text,
    View,
    Linking,
    TouchableOpacity
} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Body, Icon, Card, CardItem,Left, Button } from 'native-base';
import {connect} from 'react-redux';
import {authenticated, getAccessToken, addAuthCode} from '../actions';
import Substations from './substations';
import querystring from 'querystring';

function getOpenIdUrl(){
    const client_id = "978921633675-lsfa11bovl5nhaadrs9j12q03h64rm81.apps.googleusercontent.com";
    const authUrl = "https://accounts.google.com/o/oauth2/v2/auth";
    
    let args = 'scope=openid profile email&';
    args +='redirect_uri=urn:ietf:wg:oauth:2.0:oob&';
    args += 'client_id=' + client_id + '&';
    args += 'response_type=code';
  
    return authUrl + '?' + args;
}

class OAuth extends React.Component {
    constructor(props){
        super(props);
    }

    static navigationOptions = {
        title: "Substations"
    };
            
    componentDidMount() {
        Linking.getInitialURL().then(url =>{
            if(url){
                console.log("got url" + url);
                this.props.authenticated(querystring.parse(url.replace(/^.*\?/, '')).code);  
            }
            else if(!this.props.authentication_status.authenticated){
                Linking.openURL(getOpenIdUrl()).catch(err => console.error('An error occurred', err));
            }
        })
    };

    render() {
        return(
            <Container>
                {
                    !this.props.authentication_status.authenticated ?
            (<Content>
            <Card>
                <CardItem>
                <Input placeholder='Auth code' 
                onChangeText={(text) => this.props.addAuthCode(text)}
                value={this.props.authentication_status.auth_code}/>
                <TouchableOpacity onPress={() => this.props.getAccessToken()}>
                    <Icon name="arrow-round-forward" style={{color:'blue'}}/>                                        
                 </TouchableOpacity>
                </CardItem>
              </Card>
            </Content>) :
            (
                <Substations />
            )
                }
          </Container>
        );
    }
}

function mapStateToProps(state){
    return {
        authentication_status : state.oauth
    }
}

function mapDispatchToProps (dispatch){
    return {
        authenticated: (jwt) => dispatch(authenticated(jwt)),
        addAuthCode: (txt) => dispatch(addAuthCode(txt)),
        getAccessToken: () => dispatch(getAccessToken())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OAuth);