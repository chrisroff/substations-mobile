import React from 'react';
import {addNewSubstation,addSubstationField} from '../actions';
import {connect} from 'react-redux';
import { Container, Content, Form, Item, Input, Button, Text } from 'native-base';


class AddSubstation extends React.Component{
    constructor(props){
        super(props);
    }

    static navigationOptions = {
        title: "Add New"
    }; 

    render() {
        return (
            <Container>
            <Content>
              <Form>
                <Item>
                    <Input 
                    placeholder="Name"
                    onChangeText={(text) => this.props.addSubstationField("PrimarySubstationName",text)}
                    value={this.props.substation.field_data["PrimarySubstationName"]}/>
                </Item>
                <Item>
                    <Input 
                    placeholder="Corresponding BSP"
                    onChangeText={(text) => this.props.addSubstationField("CorrespondingBSP",text)}
                    value={this.props.substation.field_data["CorrespondingBSP"]}
                     />
                </Item>
                <Item>
                    <Input 
                    placeholder="Contact Title"
                    onChangeText={(text) => this.props.addSubstationField("ContactTitle",text)}
                    value={this.props.substation.field_data["ContactTitle"]}
                     />
                </Item>
                <Item>
                    <Input 
                    placeholder="Contact Name"
                    onChangeText={(text) => this.props.addSubstationField("ContactName",text)}
                    value={this.props.substation.field_data["ContactName"]}
                    
                     />
                </Item>
                <Item>
                    <Input 
                    placeholder="Contact Email"
                    onChangeText={(text) => this.props.addSubstationField("ContactEmail",text)}
                    value={this.props.substation.field_data["ContactEmail"]}
                    
                     />
                </Item>
                <Item>
                    <Input 
                    placeholder="Contact Tel"
                    onChangeText={(text) => this.props.addSubstationField("ContactTel",text)}
                    value={this.props.substation.field_data["ContactTel"] }
                    
                     />
                </Item>
                <Item>
                    <Input 
                    placeholder="Longitude"
                    onChangeText={(text) => this.props.addSubstationField("Longitude",text)}
                    value={this.props.substation.field_data["Longitude"]}
                    
                     />
                </Item>
                <Item>
                    <Input 
                    placeholder="Latitude"
                    onChangeText={(text) => this.props.addSubstationField("Latitude",text)}
                    value={this.props.substation.field_data["Latitude"] }
                    
                     />
                </Item>
                <Item>
                    <Input 
                    placeholder="Voltage kV"
                    onChangeText={(text) => this.props.addSubstationField("VoltagekV",text)}
                    value={this.props.substation.field_data["VoltagekV"]  }
                    
                     />
                </Item>
                <Item>
                    <Input 
                    placeholder="Transformer Nameplate Rating MVA"
                    onChangeText={(text) => this.props.addSubstationField("TransformerNameplateRatingMVA",text)}
                    value={this.props.substation.field_data["TransformerNameplateRatingMVA"] }
                    
                     />
                </Item>
                <Item>
                    <Input 
                    placeholder="Fault level kA"
                    onChangeText={(text) => this.props.addSubstationField("FaultlevelkA",text)}
                    value={this.props.substation.field_data["FaultlevelkA"]}
                    
                     />
                </Item>
                <Item>
                    <Input 
                    placeholder="Minimum Load MW"
                    onChangeText={(text) => this.props.addSubstationField("MinimumLoadMW",text)}
                    value={this.props.substation.field_data["MinimumLoadMW"]  }
                    
                     />
                </Item>
                <Item>
                    <Input 
                    placeholder="Maximum Load MW"
                    onChangeText={(text) => this.props.addSubstationField("MaximumLoadMW",text)}
                    value={this.props.substation.field_data["MaximumLoadMW"]  }
                    
                     />
                </Item>
                <Item>
                    <Input 
                    placeholder="Available Headroom Capacity MVA"
                    onChangeText={(text) => this.props.addSubstationField("AvailableHeadroomCapacityMVA",text)}
                    value={this.props.substation.field_data["AvailableHeadroomCapacityMVA"]  }
                    
                     />
                </Item>
                <Item>
                    <Input 
                    placeholder="Reverse Powerflow Capability"
                    onChangeText={(text) => this.props.addSubstationField("ReversePowerflowCapability",text)}
                    value={this.props.substation.field_data["ReversePowerflowCapability"] }
                    
                     />
                </Item>
              </Form>
            </Content>      
            <Button full
                    onPress={()=>this.props.addSubstation()}><Text>Add</Text>    
            </Button>
            </Container>
        )
    }
}

function mapStateToProps(state){
    return {
        substation: state.add_substation
    }
}

function mapDispatchToProps (dispatch){
    return {
        addSubstationField: (fieldName,value) => dispatch(addSubstationField(fieldName,value)),
        addSubstation: () => dispatch(addNewSubstation())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddSubstation);


