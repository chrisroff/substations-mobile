import React from 'react';
import {
    Button,
    Text,
    TextInput,
    View,
    StyleSheet,
    AppRegistry,
    FlatList,
    TouchableOpacity
} from 'react-native';
import SearchSubstations from './searchSubstations';
import {connect} from 'react-redux';
import {addNewSubstation, showSubstation} from '../actions';
import { NavigationActions } from 'react-navigation';
import {Container,Header,Fab, Icon, Left,Right, Body,Card,List,ListItem, CardItem, Thumbnail, Content } from 'native-base';

class Substations extends React.Component {
        constructor(props){
            super(props);

        }

        render() {
            return(
               <Container>
                <SearchSubstations/>
                <Container>
                <Content>
                <List style={{backgroundColor:'white'}}>
                
                    {
                    this.props.substations_query.substations.map((substation,i) => {
                       
                           return ( 
                            
                            <TouchableOpacity key={i}>
                            <ListItem onPress={()=>this.props.showSubstation(substation)}>
                    
                            {
                                substation.TransmissionStatus === 'Constrained' ? 
                                <Icon style={{color:'red'}} name='disc'/> :
                                <Icon style={{color:'green'}}name='disc'/>
                            }
                            <Text>       </Text>
                            <Body>
                            <List>
                                    <Text style={{fontWeight:'bold', fontSize:15}}> {substation.PrimarySubstationName}</Text>
                                    <Text> {substation.ContactName}</Text>
                                    <Text> {substation.ContactTel}</Text>
                            </List>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>

                        </ListItem>
                        </TouchableOpacity>
                        
                        )
                     })
                    }
                    </List>
                    </Content>
                    <Fab
                    style={{ backgroundColor: '#5067FF' }}
                    position="topRight"
                    onPress={this.props.addNew}>
                    <Icon name="add" />
                </Fab>
                    </Container>
           </Container>)
        }
        
    }

function mapStateToProps(state){
    return {
        substations_query : state.substations,
    }
}

function mapDispatchToProps (dispatch){
    return {
        fetch: () => dispatch(fetchSubstationsFromAPI()),
        addNew: () => dispatch(NavigationActions.navigate({ routeName: 'Add' })),
        showSubstation: (substation) =>{
            dispatch(showSubstation(substation));
            dispatch(NavigationActions.navigate({ routeName: 'Show' }));
        } 
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Substations);