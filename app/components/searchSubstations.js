import React, { Component } from 'react';
import  { View } from 'react-native';
import { Container, Header, Item, Input, Icon, Button, Text } from 'native-base';
import {connect} from 'react-redux';
import {fetchSubstationsFromAPI, setSearch} from '../actions';

class SubstationsSearch extends Component {
    constructor(props){
        super(props);

    }

  render() {
    return (
      <Header searchBar rounded>
          <Item>
            <Icon name="search" />
            <Input placeholder="Search"
                   onChangeText={(text) => this.props.search(text)}
                   value={this.props.search_change.search_text}
             />
          </Item>
      </Header>
    );}
}   


function mapStateToProps(state){
    return {
        search_change : state.search
    }
}

function mapDispatchToProps (dispatch){
    return {
        search: (search) => {
            dispatch(setSearch(search));
            dispatch(fetchSubstationsFromAPI());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SubstationsSearch);