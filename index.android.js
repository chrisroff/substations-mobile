import React from 'react';
import {AppRegistry} from 'react-native';
import {Provider} from 'react-redux';
import configureStore from './app/configureStore';
import { addNavigationHelpers } from "react-navigation";
import AppWithNavigationState from './app/navigation';
import OAuth from './app/components/oauth';

const store = configureStore();

const SubstationsMobile = () =>{
  return (
  <Provider store={store}>
    <AppWithNavigationState/>
  </Provider>
  );
}

AppRegistry.registerComponent('substationsmobile', () => SubstationsMobile);
